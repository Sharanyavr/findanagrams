words = {}
for line in open("sowpods.txt"):
    word = line.strip();
    #sorted(string) returns a list
    letters = ''.join(sorted(word))
    if letters not in words:
        words[letters] = []
    words[letters].append(word)

for letters in words:
    if len(words[letters]) > 1:
        print(words[letters])
